package com.learn.test.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
//import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.learn.test.model.User;
import com.learn.test.service.EmployeeService;

@Controller
@RequestMapping("/emp")
public class EmployeeController {
	
	
	@Autowired
	private EmployeeService employeeService;

	@GetMapping("/test1") // method level
	public @ResponseBody String test() {
		return "this is test for ResponseBody!!!";
	}

	@GetMapping("/index")
	public String showUserList(Model model) {

		 model.addAttribute("users", employeeService.fetchAllEmployees());
		return "index";
	}

	@GetMapping("/signup")
	public String showSignUpForm(User user) {
		return "sign-up";
	}

	@PostMapping("/adduser")
	public String addUser(@Valid User user, BindingResult result, Model model) {
		if (result.hasErrors()) {
			return "sign-up";
		}
		//list.add(user);
		employeeService.save(user);
		return "redirect:/emp/index";
	}

	@ModelAttribute
	public void addAttributes(Model model) {
		model.addAttribute("msg", "Welcome to the SpringBoot-Thymeleaf!");
	}

}
