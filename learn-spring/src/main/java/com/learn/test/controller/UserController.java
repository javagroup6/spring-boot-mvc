package com.learn.test.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.learn.test.model.User;

@Controller
public class UserController {
	private static  List<User> list=new ArrayList();
	
	@RequestMapping("/fetch-all-users")
    public String showReadContactPage(Model model) {
        model.addAttribute("users", list);
        return "all-users";
    }

    @RequestMapping("/create-user")
    public String showCreateContactPage(Model model) {
        model.addAttribute("command", new User());
        return "create-user";
    }

    @RequestMapping(value = "/create-user", method = RequestMethod.POST)
    public String createContact(@ModelAttribute("user") User user) {
    	  list.add(user);
        return "redirect:/fetch-all-users";
    }

    

}
