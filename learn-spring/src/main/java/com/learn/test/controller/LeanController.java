package com.learn.test.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.learn.test.model.User;

@RestController
@RequestMapping("/learn") // class level mapping
public class LeanController {
	
	
	@GetMapping("/test/{name}")// method level
	public ResponseEntity<?> test(@RequestParam int id,@PathVariable("name") String name){
		ResponseEntity response=null;
		String res=null;
		int num=10;
		float f=20;
		if(id==1){
			res="test";
			response=ResponseEntity.ok().body(res+name);
		}
		if(id==2){
			f=78;
			response=ResponseEntity.ok().body(f+name);
		}
		
		if(id==2){
			num=89;
			response=ResponseEntity.ok().body(num+name);
		}
		
		return response;
		
	}
	
	
}
