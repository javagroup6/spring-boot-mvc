package com.learn.test.exeptions;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionController extends ResponseEntityExceptionHandler {
	
	
	@ExceptionHandler(IDNotFoundException.class)
	public ResponseEntity<?> handleIdnotFounException(IDNotFoundException e){
		
		  Error error = new Error();
		  error.setDate(new Date());
		  error.setHttpStatus(HttpStatus.NOT_FOUND);
		  error.setMessage(e.getMessage());
		  
		  return new ResponseEntity<>(error, error.getHttpStatus());
	}
	
	

}
