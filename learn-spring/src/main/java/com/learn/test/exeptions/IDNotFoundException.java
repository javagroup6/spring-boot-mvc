package com.learn.test.exeptions;

public class IDNotFoundException extends RuntimeException{
	
	public IDNotFoundException(String msg){
		super(msg);
		
	}

}
