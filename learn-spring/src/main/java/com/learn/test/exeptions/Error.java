package com.learn.test.exeptions;

import java.util.Date;

import org.springframework.http.HttpStatus;

public class Error {
    private  HttpStatus httpStatus;
    private  String message;
    private Date date;
	public HttpStatus getHttpStatus() {
		return httpStatus;
	}
	public String getMessage() {
		return message;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}
	public void setMessage(String message) {
		this.message = message;
	}
    
    
}