package com.learn.test.service;

import java.util.List;

import com.learn.test.entity.Employee;
import com.learn.test.model.User;

public interface EmployeeService {
	
	Employee save(User user);
	
	Employee update(User user);
	Employee search(int id);
	void delete(int id);
	List<Employee> fetchAllEmployees();
	

}
