package com.learn.test.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.learn.test.entity.Employee;
import com.learn.test.model.User;
import com.learn.test.repository.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {
	
	@Autowired
	private EmployeeRepository employeeRepository;

	@Override
	public Employee save(User user) {
		Employee emp=new Employee();
		emp.setName(user.getName());
		emp.setEmail(user.getEmail());
		return employeeRepository.save(emp);
	}

	@Override
	public Employee update(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Employee search(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Employee> fetchAllEmployees() {
		return employeeRepository.findAll();
	}

}
