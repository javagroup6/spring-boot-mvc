package com.hibernate.hql;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.hibernate.sample.HibernateUtil;
import com.hibernate.sample.entity.Student;

public class HqlDemo {

	public static void main(String[] args) {
		
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
    	Session session=sessionFactory.getCurrentSession();
    	Transaction transaction = null;
    	try{
    		transaction=session.beginTransaction();
    		/*String s="from Student";
    		Query<Student> query=session.createQuery(s);
    		List<Student> list =query.list();
    		for(Student student:list){
    			System.out.println(student.getEmail()+":::"+student.getFirstName());
    			
    		}*/
    		
    		/*String updateStudent="update Student s set s.firstName=:newFirstName where s.id=:studentId";
    		
    		Query<Student> query=session.createQuery(updateStudent);
    		query.setParameter("newFirstName", "ram");
    		query.setParameter("studentId", 1l);
    		int effectRows=query.executeUpdate();
    		System.out.println(effectRows+"  ::: rows are updated ...");*/
    		
    		String deleteQuery="delete Student s where s.id=:id";
    		Query<Student> query=session.createQuery(deleteQuery);
    		query.setParameter("id", 1l);
    		int effectRows=query.executeUpdate();
    		System.out.println(effectRows+"  ::: rows are deleted ...");
    		transaction.commit();
    		
    	}catch(Exception e){
    		transaction.rollback();
    		System.out.println(e.getMessage());
    	}finally {
            if (session != null) {
                session.close();
            }
            if(sessionFactory!=null){
            	sessionFactory.close();
            }
	}

}
}
