package com.hibernate.cache;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.hibernate.sample.HibernateUtil;
import com.hibernate.sample.entity.Student;

import net.sf.ehcache.CacheManager;

public class SecondLevelCacheExample {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Transaction transaction = null;
		//Session session = HibernateUtil.getSessionFactory().openSession();
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
    	//Session session=sessionFactory.getCurrentSession();
		 Session session1=null;
		 Session session2=null;
		 Session session3=null;
		// Transaction transaction = null;
		 try{
			 int size = CacheManager.ALL_CACHE_MANAGERS.get(0)
					  .getCache("com.hibernate.sample.entity.Student").getSize();
			 System.out.println("before cache :"+size);
			session1= sessionFactory.openSession();
		    Student emp1=(Student)session1.load(Student.class,1l);    
		    System.out.println(emp1.getId()+" "+emp1.getFirstName()+" "+emp1.getLastName());    
		    session1.close(); 
		    int size1 = CacheManager.ALL_CACHE_MANAGERS.get(0)
					  .getCache("com.hibernate.sample.entity.Student").getSize();
		    System.out.println("after cache :"+size1);   
		    session2= sessionFactory.openSession();
		    Student emp2=(Student)session2.load(Student.class,1l);    
		    System.out.println(emp2.getId()+" "+emp2.getFirstName()+" "+emp2.getLastName()); 
		    int size2 = CacheManager.ALL_CACHE_MANAGERS.get(0)
					  .getCache("com.hibernate.sample.entity.Student").getSize();
		    System.out.println("after second session cache status :"+size2);
		    session2.close();  
		    //sessionFactory.close();
		    session3= HibernateUtil.getSessionFactory().openSession();
		    Student emp3=(Student)session3.load(Student.class,1l);    
		    System.out.println(emp3.getId()+" "+emp3.getFirstName()+" "+emp3.getLastName()); 
		   // transaction.commit();
	}catch(Exception e){
		//transaction.rollback();
		System.out.println(e.getMessage());
	}finally {
        if (session1 != null) {
            session1.close();
        }
        if (session2 != null) {
            session2.close();
        }
        if(sessionFactory!=null){
        	sessionFactory.close();
        }
		
	}
	}

}