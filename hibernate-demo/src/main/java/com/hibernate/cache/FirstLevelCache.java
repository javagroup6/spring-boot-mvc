package com.hibernate.cache;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.hibernate.sample.HibernateUtil;
import com.hibernate.sample.entity.Student;

public class FirstLevelCache {

	public static void main(String[] args) {
		Transaction transaction = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
        try  {

            // start the transaction
            transaction = session.beginTransaction();

            // get the student entity using id
            Student student1 = session.load(Student.class, 1l);// checks in session ,2 sessionfactory, 3 databse

            System.out.println(student1.getFirstName());
            System.out.println(student1.getLastName());
            System.out.println(student1.getEmail());
            session.evict(student1); //initially it will use same session object 
            //if we evit student object from session then it will trigger one more time to hit database
            // load student entity by id
            System.out.println("========================================");
            Student student2 = session.load(Student.class, 1l);// checks in session 
            System.out.println(student2.getFirstName());
            System.out.println(student2.getLastName());
            System.out.println(student2.getEmail());
          
            // commit transaction
            transaction.commit();

        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            System.out.println("exception occured:"+e.getMessage());
        }finally {
            if (session != null) {
                session.close();
            }
            

        }
	}

}
