package com.hibernate.sample;





import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.hibernate.sample.entity.Student;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
    	Session session=sessionFactory.getCurrentSession();
    	//s.beginTransaction(); // ACID properties : Automacity,consistency,isolation,durability 	
    	Transaction transaction = null;
        try  {
            // start a transaction
            transaction = session.beginTransaction();
            // save the student objects
            Student student=new Student();// tranciant state
        	student.setFirstName("ramesh");
        	student.setLastName("sorum");
        	student.setEmail("ramesh@gamil.com");
        	student.setSalary(10000);
        	session.save(student); // persistance state
            // commit transaction
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            if(session!=null){
            	session.close(); // detached state
            }
            if(sessionFactory!=null){
            	sessionFactory.close();
            }
            e.printStackTrace();
        }
    	
    }
}
