package com.hibernate.sample.dao;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.hibernate.sample.HibernateUtil;
import com.hibernate.sample.onetoone.InstructorDetail;
public class InstructorDetailDao {

    public void saveInstructorDetail(InstructorDetail instructorDetail) {
        Transaction transaction = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try  {
            // start a transaction
            transaction = session.beginTransaction();
            // save the student object
            session.save(instructorDetail);
            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public void updateInstructorDetail(InstructorDetail instructorDetail) {
        Transaction transaction = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            // start a transaction
            transaction = session.beginTransaction();
            // save the student object
            session.update(instructorDetail);
            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public InstructorDetail getInstructorDetail(int id) {

        Transaction transaction = null;
        InstructorDetail instructor = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try  {
            // start a transaction
            transaction = session.beginTransaction();
            // get an instructor object
            instructor = session.get(InstructorDetail.class, id);
            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return instructor;
    }
}