package com.hibernate.sample.onetoone;


import com.hibernate.sample.dao.InstructorDao;

public class ManApp {
    public static void main(String[] args) {

        Instructor instructor = new Instructor("Suresh", "rajiv", "suresh@gamil.com");
        InstructorDetail instructorDetail = new InstructorDetail(".Net", "Listing Music");
        instructor.setInstructorDetail(instructorDetail);

       InstructorDao instructorDao = new InstructorDao();
        instructorDao.saveInstructor(instructor);
        
    }
}