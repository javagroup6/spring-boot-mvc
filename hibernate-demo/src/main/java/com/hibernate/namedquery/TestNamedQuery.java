package com.hibernate.namedquery;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.annotations.NamedNativeQuery;
import org.hibernate.query.Query;

import com.hibernate.sample.HibernateUtil;
import com.hibernate.sample.entity.Student;

public class TestNamedQuery {

	public static void main(String[] args) {
		
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
    	Session session=sessionFactory.getCurrentSession();
    	Transaction transaction = null;
    	try{
    		transaction=session.beginTransaction();
    	// NamedQuery 	
    	/*Query query=session.getNamedQuery("fetch_allStudents");
    	
    	List<Student> list =query.list();
		for(Student student:list){
			System.out.println(student.getEmail()+":::"+student.getFirstName());
			
		}*/
    		
    	// Native Named Queries
    		
    		Query query=session.getNamedNativeQuery("fetch_allStudentsByNative");
    		List<Student> list =query.list();
    		for(Student student:list){
    			System.out.println(student.getEmail()+":::"+student.getFirstName());
    		}
    		
    		
    		transaction.commit();

    	}catch(Exception e){
    		transaction.rollback();
    		System.out.println(e.getMessage());
    	}finally {
            if (session != null) {
                session.close();
            }
            if(sessionFactory!=null){
            	sessionFactory.close();
            }
	}

}

}
