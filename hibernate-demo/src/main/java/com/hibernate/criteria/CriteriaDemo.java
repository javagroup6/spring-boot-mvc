package com.hibernate.criteria;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.query.Query;

import com.hibernate.sample.HibernateUtil;
import com.hibernate.sample.entity.Student;

public class CriteriaDemo {

	public static void main(String[] args) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
    	Session session=sessionFactory.getCurrentSession();
    	Transaction transaction = null;
    	try{
    		transaction=session.beginTransaction();
    		
    		// old style -> before 5.2 
    		
    		//Criteria cr=session.createCriteria(Student.class);
    		//cr.addOrder(Order.asc("name"));
    		
    		// after 5.2 version
    		CriteriaBuilder cb = session.getCriteriaBuilder();
    		CriteriaQuery<Student> cr = cb.createQuery(Student.class);
    		Root<Student> root = cr.from(Student.class);
    	//	cr.select(root);  // senario 1 select * from Student;
    	//	cr.select(root).where(cb.like(root.<String>get("firstName"),"%ra%")); // senario 2
    	//  cr.select(root).where(cb.gt(root.<Integer>get("salary"),1000));	// senario 3
        //     cr.select(root).where(cb.between(root.<Integer>get("salary"),1000,15000));  // senario 4
             
             
    		Predicate greaterThanPrice = cb.gt(root.<Integer>get("salary"),1000);
    		Predicate namematching= cb.like(root.<String>get("firstName"),"%ra%");
    		//cr.select(root).where(cb.or(greaterThanPrice, namematching));
    		//cr.select(root).where(cb.and(greaterThanPrice, namematching)); 
    		
    		// sorting 
    		
    	/*	cr.orderBy(cb.asc(root.get("firstName")),
    	    cb.desc(root.get("salary")));*/
    		
    		// projections
    		
    		CriteriaBuilder builder = session.getCriteriaBuilder();
    		CriteriaQuery<Object[]> query1 = builder.createQuery(Object[].class);
    		Root<Student> student = query1.from(Student.class);
    		//query1.multiselect(student.get("firstName"), builder.count(student));
    	    query1.multiselect(student.get("firstName"), student.get("lastName"),student.get("salary"));
    		query1.groupBy(student.get("firstName"));
    		
    		Query<Object[]> query = session.createQuery(query1);
    		List<Object[]> results = query.getResultList();
    		
    		//System.out.println(results.get(0)[0]);
    		
    		for(Object[] obj:results){
    			System.out.println("the firstName :"+obj[0]+" ::count :"+obj[1]+" salary:"+obj[2]);
    		}
             
    		/*Query<Student> query = session.createQuery(cr);
    		List<Student> results = query.getResultList();
    		for(Student s:results){
    			System.out.println(s.toString());
    		}*/
            transaction.commit();
    		
    	}catch(Exception e){
    		transaction.rollback();
    		System.out.println(e.getMessage());
    	}finally {
            if (session != null) {
                session.close();
            }
            if(sessionFactory!=null){
            	sessionFactory.close();
            }
	}

}
}
