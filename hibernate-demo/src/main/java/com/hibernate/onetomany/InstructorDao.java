package com.hibernate.onetomany;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.hibernate.sample.HibernateUtil;



public class InstructorDao {
 public void saveInstructor(Professor instructor) {
  Transaction transaction = null;
  Session session = HibernateUtil.getSessionFactory().openSession();
  try  {
   // start a transaction
   transaction = session.beginTransaction();
   // save the student object
   session.save(instructor);
   // commit transaction
   transaction.commit();
  } catch (Exception e) {
   if (transaction != null) {
    transaction.rollback();
   }
   e.printStackTrace();
  }
  finally {
      if (session != null) {
          session.close();
      }
    
 }
 }
 public void updateInstructor(Professor instructor) {
  Transaction transaction = null;
  try (Session session = HibernateUtil.getSessionFactory().openSession()) {
   // start a transaction
   transaction = session.beginTransaction();
   // save the student object
   session.update(instructor);
   // commit transaction
   transaction.commit();
  } catch (Exception e) {
   if (transaction != null) {
    transaction.rollback();
   }
   e.printStackTrace();
  }
 }

 public void deleteInstructor(int id) {

  Transaction transaction = null;
  try (Session session = HibernateUtil.getSessionFactory().openSession()) {
   // start a transaction
   transaction = session.beginTransaction();

   // Delete a instructor object
   Professor instructor = session.get(Professor.class, id);
   if (instructor != null) {
    session.delete(instructor);
    System.out.println("instructor is deleted");
   }

   // commit transaction
   transaction.commit();
  } catch (Exception e) {
   if (transaction != null) {
    transaction.rollback();
   }
   e.printStackTrace();
  }
 }

 public Professor getInstructor(int id) {

  Transaction transaction = null;
  Professor instructor = null;
  Session session = HibernateUtil.getSessionFactory().openSession();
  try {
   // start a transaction
   transaction = session.beginTransaction();
   // get an instructor object
   instructor = session.get(Professor.class, id);
   // commit transaction
   System.out.println("the details :"+instructor.toString());
   transaction.commit();
  } catch (Exception e) {
		   if (transaction != null) {
		    transaction.rollback();
		   }
		   e.printStackTrace();
		  }
		  finally {
		      if (session != null) {
		          session.close();
		      }
 
 }
  return instructor;
 }
}