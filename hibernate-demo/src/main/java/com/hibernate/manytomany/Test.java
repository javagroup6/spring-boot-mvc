package com.hibernate.manytomany;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.Session;

import com.hibernate.sample.HibernateUtil;



public class Test {
    public static void main(String[] args) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        // Create an employee
        Employee employee = new Employee();
        employee.setFirstName("Suraj");
        employee.setLastName("reku");
        
        Employee employee2 = new Employee();
        employee2.setFirstName("Venkat");
        employee2.setLastName("nika");

       // Create project1
        Project project = new Project();
        project.setTitle("Banking Project");

        // Create project2
        Project project1 = new Project();
       project1.setTitle("Insurence Project");
       
       Project project3 = new Project();
       project3.setTitle("Finance Project");
       
       Project project4 = new Project();
       project4.setTitle("E-comerce");

       // employee can work on two projects, Add project references in the employee
      // employee.getProjects().add(project);
      // employee.getProjects().add(project1);

        // Add employee reference in the projects
       //project.getEmployees().add(employee);
       //project1.getEmployees().add(employee);
       Set<Project> empProjects=new HashSet();
       empProjects.add(project3);
       empProjects.add(project4);
       employee.setProjects(empProjects);
       Set<Project> emp2Projects=new HashSet();
       emp2Projects.add(project1);
       emp2Projects.add(project);
     //  employee.setProjects(masthansetProjects);
       employee2.setProjects(emp2Projects);
       session.save(employee);
       session.save(employee2);
       session.getTransaction().commit();
       HibernateUtil.close();
       
       
       
                 }
}